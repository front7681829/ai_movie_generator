const form = document.querySelector('form');
const META_API_URL = 'http://localhost:8080/api/meta';
const IMAGE_API_URL = 'http://localhost:8080/api/image';
form.addEventListener('submit', async (e) => {
    e.preventDefault();
    showSpinner();

    const data = new FormData(form);

    try {
        const descriptionResponse = await fetch(META_API_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title: data.get('title')
            })
        })

        const imageResponse = await fetch(IMAGE_API_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title: data.get('title')
            })
        });

        if (!descriptionResponse.ok || !imageResponse.ok) {
            throw new Error('Server connection error');
        }

        const { description } = await descriptionResponse.json();
        const descriptionText = description.content;
        const { image } = await imageResponse.json();

        const descriptionContainer = document.querySelector('#description');
        const imageContainer = document.querySelector('#image');

        descriptionContainer.innerHTML = `
            <h2>Script</h2>
            <p>${descriptionText.split("\n").join("<br />")}</p>
        `;
        imageContainer.innerHTML = `
            <h2>Cover</h2>
            <img src="${image}" width="512"  alt="image generated by OpenAI"/>
        `;

        hideSpinner();
    } catch (error) {
        console.log(error)
        hideSpinner();
    }
});

function showSpinner() {
    const button = document.querySelector('#generate');
    button.disabled = true;
    button.innerHTML = 'Loading... <span class="spinner">🗿</span>';
}

function hideSpinner() {
    const button = document.querySelector('#generate');
    button.disabled = false;
    button.innerHTML = 'Generate';
}