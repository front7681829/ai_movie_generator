import { openai } from '../config/openAIConfig.js';

export async function generateMeta(req, res) {
    try {
        const { title } = req.body;

        const description = await openai.createChatCompletion({
            model: 'gpt-3.5-turbo',
            messages: [
                {
                    role: 'user',
                    content: `Create the script for a movie with this title '${title}'`,
                }
            ],
            max_tokens: 100,
        });

        res.status(200).json({
            description: description.data.choices[0].message,
        });
    } catch (error) {
        console.log(error);
        res.status(500).send(error?.response.data.error.message ?? 'Unexpected error');
    }
}

export async function generateImage(req, res) {
    try {
        const {title} = req.body;
        const prompt = `Cover for a movie with the following name '${title}'.`;

        const image = await openai.createImage({
            prompt,
            n: 1,
            size: "1024x1024",
        });

        res.status(200).json({
            image: image.data.data[0].url,
        });
    } catch (error) {
        console.log(error);
        res.status(500).send(error?.response.data.error.message ?? 'Unexpected error');
    }
}