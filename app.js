import {generateImage, generateMeta} from './controllers/openAIController.js';
import express from 'express';
import cors from 'cors';

/*
App setup
 */
const app = express();
app.listen(8080, () => console.log('Generate scripts and movie covers on port 8080...'));

/*
middleware
 */
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

/*
routes
 */
app.post('/api/meta', generateMeta);
app.post('/api/image', generateImage);
