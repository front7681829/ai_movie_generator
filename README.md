<!-- TOC -->
* [AI Image Generator](#ai-image-generator)
* [Setup](#setup)
  * [Linux](#linux)
<!-- TOC -->
# AI Image Generator
Web application that generates the script and cover of a movie from the title provided.

![img.png](preview.png)
# Setup
## Linux
- install dependencies
  ```bash
  cd ai_movie_generator && \
  npm install
  ```
- Create an .env file with your OpenAI key.
    ```bash
    # in the root of the project
    KEY="your_openai_key" && \
    cat <<EOF >./.env
    OpenAIKey="${KEY}"
    EOF
    ```
- run backend
  ```bash
  node app
  ```